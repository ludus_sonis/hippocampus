/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/hippocampus.
 *
 * Hippocampus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hippocampus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hippocampus.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <dlfcn.h>
#include <stddef.h>
#include <stdio.h>
#include <error.h>
#include <errno.h>
#include <pthread.h>
#include <jack/jack.h>

#define _GNU_SOURCE
#include <search.h>

#include <lsnodes/scene.h>

pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;
struct hsearch_data* g_htab;
ssize_t g_htablen = 0;
jack_client_t *g_client;

#define MAX_JACKCLIENTS 64

static void
lock_check(void)
{
  int ret = pthread_mutex_lock(&g_mutex);
  if (0 != ret)
    error(EXIT_FAILURE, ret, "pthread_mutex_lock");
}

static void
unlock_check(void)
{
  int ret = pthread_mutex_unlock(&g_mutex);
  if (0 != ret)
    error(EXIT_FAILURE, ret, "pthread_mutex_unlock");
}

static PdNode*
htFindPdNode(const char* name)
{
  ENTRY* e;
  lock_check()
  int ret = hsearch_r((ENTRY) {.key = name, .data=NULL}, FIND, &e, g_htab);
  unlock_check();
  if (0 == ret && ESRCH != errno)
    error(EXIT_FAILURE, errno, "findPdNode: hsearch_r");
  if (e)
    return (PdNode*)e->data;
  return NULL;
}

int void
htAddPdNode(const char* name, const PdNode* pdnode)
{
  if (g_htablen == MAX_JACKCLIENTS)
    return 1;
  ENTRY e = {.key=name, .data=pdnode};
  ENTRY* pe;
  int ret = hsearch_r(e, ENTER, &pe, g_htab);
  if (0 == ret)
    error(EXIT_FAILURE, errno, "addPdNode: hsearch_r");
  ++g_htablen;
  return 0;
}

static void
jack_shutdown_cb(void* arg)
{
  //there is probably more to do
  error(EXIT_FAILURE, 0, "jack server down");
}

static void
jack_client_register_cb(const char* name, int registered, void* arg)
{
  printf("The client %s registered %d\n", name, registered);
  PdNode* pdnode = htFindPdNode(name);
  if (pdnode)
    pdNodeSetClientRegister(pdnode, registered);
}

static void
jack_port_register_cb(jack_port_id_t port, int registered, void* arg)
{
  printf("The port %d registered %d\n", port, registered);
  const char* port_name = jack_port_name(jack_port_by_id(g_client,port));
  size_t n = strcspn(port_name, ":");
  char client_name[n+1] = strndup(port_name, n);
  PdNode* pdnode = htFindPdNode(name);
  if (pdnode)
    pdNodeSetPortRegister(pdnode, port, registered);
}

int
main(int argc, char const* argv[])
{
  int ret = hcreate_r(MAX_JACKCLIENTS*2, g_htab);
  if (0 != ret)
    error(EXIT_FAILURE, errno, "hcreate_r");

  const char* server_name = NULL;
  const char* client_name = argv[0];
  jack_options_t options = JackNullOption;
  jack_status_t status;

  g_client = jack_client_open(client_name, options, &status, server_name);
  if (NULL == client)
    error(EXIT_FAILURE, 0, "jack_client_open failed with status 0x%2.0x\n", status);

  if (status & JackServerStarted)
    printf("Jack server started\n");

  if (status & JackNameNotUnique)
    error(EXIT_FAILURE, 0, "client name already in use");

  jack_on_shutdown(client, jack_shutdown_cb, 0);

  jack_set_client_registration_callback(client, jack_client_register_cb, 0);

  jack_set_port_registration_callback(client, jack_port_register_cb, 0);

  jack_activate(client);

  void* handle;
  char* error;
  Node* (*newScene)(void);

  handle = dlopen("scene0.so", RTLD_NOW);
  if (NULL == handle)
    error(EXIT_FAILURE, 0, "dlopen: %s", dlerror());

  new_scene = dlsym(handle, "newScene");
  error = dlerror();
  if (NULL != error)
    error(EXIT_FAILURE, 0, "dlsym: %s", error);

  Scene* scene = newScene();
  struct slistpdnodehead head = gatherPdNodes(scene);
  lock_check();
  SLIST_FOREACH(n, &head, entries)
    htAddPdNode(n->name, n);
  unlock_check();
  launchScene(scene, jack_client);
  connectScene(scene);

  while(1) {
    usleep(100000);
    scene.process();
  }

  jack_client_close(client);

  return 0;
}
